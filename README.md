# Catodon Ubuntu install shell script

---

⚠️ **WIP - NOT FULLY WORKING YET!** ⚠️

---

This is forked from <https://codeberg.org/catodon/ubuntu-bash-install> for use with Catodon.

Install Catodon with one shell script!

You can install catodon on an Ubuntu server just by answering some questions.

There is also an update script.

## Requirements

- A domain
- An Ubuntu server/vps
- Git
- wget
- Docker (optional)

## Configure Cloudflare

If you are using Nginx and Cloudflare, you must configure Cloudflare:

- Set DNS.
- On SSL/TLS setting tab, switch the encryption mode to "Full".
- Turn off code minification.

## Steps

### 1. Prepare

Make sure all packages are up to date and reboot.

```sh
sudo apt update; sudo apt install git wget; sudo apt full-upgrade -y; sudo reboot
```

### 2. Start the installation

Reconnect SSH and let's start installing Catodon.

```sh
wget https://codeberg.org/catodon/ubuntu-bash-install/raw/branch/main/ubuntu.sh -O ubuntu.sh; sudo bash ubuntu.sh
```

### 3. Updating

There is also an update script.

First, download the script.

```sh
wget https://codeberg.org/catodon/ubuntu-bash-install/raw/branch/main/update.ubuntu.sh -O update.sh
```

Run it when you want to update Catodon.

```sh
sudo bash update.sh
```

- In the systemd environment, the `-r` option can be used to update and reboot the system.
- In the docker environment, you can specify repository:tag as an argument.
